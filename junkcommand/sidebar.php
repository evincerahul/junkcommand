<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div class="blog_search">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</div>
<div class="blog_category">
  <?php if(function_exists('dynamic_sidebar')) {
    ob_start();
    dynamic_sidebar('category-sidebar');
    $sidebar = ob_get_contents();
    ob_end_clean();
    $sidebar_corrected_ul = str_replace("<ul>", '<ul class="">', $sidebar);
    echo $sidebar_corrected_ul;
    } 
  ?>
</div>
<div class="blog_popular_post">
	<h3>Popular post</h3>
	<?php 
	$popularpost = new WP_Query( array( 'posts_per_page' => 3, 'meta_key' => 'wpb_post_views_count', "orderby" => "modified", 'order' => 'DESC'  ) );
	while ( $popularpost->have_posts() ) : $popularpost->the_post();	?>
		<div class="popular_post_content">
			<div class="post_iumg">
				<a href="<?php echo get_permalink($popularpost->ID); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/blog_post1.jpg"></a>
			</div>
			<div class="post_content"><a href="<?php echo get_permalink($popularpost->ID); ?>"><?php	the_title(); ?></a> <span><?php the_time('j M'); ?></span> </div>
		</div>
	<?php 	endwhile;	?>
</div>
<div class="blog_popular_post">
	<h3>Latest post</h3>
	<?php 
	$Latestpost = new WP_Query( array( 'posts_per_page' => 3, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
	while ( $Latestpost->have_posts() ) : $Latestpost->the_post();	?>
		<div class="popular_post_content">
			<div class="post_iumg">
				<a href="<?php echo get_permalink($Latestpost->ID); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/blog_post1.jpg"></a>
			</div>
			<div class="post_content"><a href="<?php echo get_permalink($Latestpost->ID); ?>"><?php	the_title(); ?></a> <span><?php the_time('j M'); ?></span> </div>
		</div>
	<?php 	endwhile;	?>
</div>
