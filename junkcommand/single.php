<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<section class="green_box_title">
	  <div class="container">
		<div class="row">
		  <div class="col-md-12">
			<div class="recycling"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/recycal.png" alt=""></div>
		  </div>
		  <div class="col-md-12">
		   <h1><?php _e( get_the_title(), 'twentyseventeen' ); ?></h1>
		 </div>
	   </div>
	 </div>
</section>
<section class="blog_section inner_blog_section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 blog_left_containt">
				<?php
				/* Start the Loop */
				while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/post/content', get_post_format() );
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				endwhile; // End of the loop.
				?>
			</div>
			<div class="col-md-4 blog_right_containt">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>

</div><!-- .wrap -->

<?php get_footer();
