<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div class="comment_box_section">
	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) : ?>
	
		<h4>
			<?php
			$comments_number = get_comments_number();
			if ( '1' === $comments_number ) {
				/* translators: %s: post title */
				
				//printf( _x( 'One Reply to &ldquo;%s&rdquo;', 'comments title', 'twentyseventeen' ), get_the_title() );
			} else {	?>
				Comments(<?php	if($post->comment_count!=""){ echo $post->comment_count;	} else {	echo "0";	} 	?>)
			<?php	}	?>
		</h4>

		<ol class="comment-list">
			<?php
				wp_list_comments( array(
					'avatar_size' => 100,
					'style'       => 'ol',
					'short_ping'  => true,
					'reply_text'  => twentyseventeen_get_svg( array( 'icon' => 'mail-reply' ) ) . __( 'Reply', 'twentyseventeen' ),
				) );
			?>
		</ol>

		<?php the_comments_pagination( array(
			'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous', 'twentyseventeen' ) . '</span>',
			'next_text' => '<span class="screen-reader-text">' . __( 'Next', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
		) );

	endif; // Check for have_comments().

	// If comments are closed and there are comments, let's leave a little note, shall we?
	if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>

		<p class="no-comments"><?php _e( 'Comments are closed.', 'twentyseventeen' ); ?></p>
	<?php
	endif;

	//comment_form();
	
	?>
	<div id="respond" class="leave_comments_form">
		<h5>Leave a Reply <small><a rel="nofollow" id="cancel-comment-reply-link" href="/junkcommand/downsizing-and-dont-know-what-to-do-with-all-of-your-stuff/#respond" style="display:none;">Cancel reply</a></small></h5>	
		<form action="<?php echo site_url(); ?>/wp-comments-post.php" method="post" id="commentform" class="comment-form" novalidate="">
			<div class="form_grp">
				<div class="row">
				  <div class="col-md-6">
						<input id="author" name="author" size="30" maxlength="245" aria-required="true" required="required" type="text" value="<?php echo $comment_author; ?>" placeholder="Name" class="form_controlar">
				  </div>
				  <div class="col-md-6">
						<input id="url" name="url" value="" size="30" maxlength="200" type="url" value="<?php echo $comment_author_url; ?>" class="form_controlar" placeholder="Subject">
					</div>
				</div>
			</div>
			<div class="form_grp">
			   <div class="row">
				<div class="col-md-12">
					<input id="email" name="email" value="<?php echo $comment_author_email; ?>" size="30" maxlength="100" aria-describedby="email-notes" aria-required="true" required="required" type="email" class="form_controlar" placeholder="Email">
				</div>
			  </div>
			</div>
			 <div class="form_grp">
			  <div class="row">
				<div class="col-md-12">
				 <textarea id="comment" name="comment" cols="45" rows="8" maxlength="65525" aria-required="true" required="required" class="form_controlar" placeholder="Comment"></textarea>
				</div>
			  </div>
			</div>
			 <div class="form_grp">
				<div class="row">
					<div class="col-md-4">
						<input name="submit" id="submit" class="btn" value="Submit" type="submit" >
						<input name="comment_post_ID" value="<?php	echo the_ID();	?>" id="comment_post_ID" type="hidden">
						<input name="comment_parent" id="comment_parent" value="<?php	echo wp_get_post_parent_id( $post_ID ); 	?>" type="hidden">
					</div>
				</div>
			</div>
			<p style="display: none;">
				<input id="akismet_comment_nonce" name="akismet_comment_nonce" value="8064760ac8" type="hidden">
			</p>
			<p style="display: none;"></p>
			<input id="ak_js" name="ak_js" value="1507115562002" type="hidden">
		</form><!-- #comments -->
	</div><!-- #comments -->
</div>