<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
	<section class="green_box_title">
	  <div class="container">
		<div class="row">
		  <div class="col-md-12">
			<div class="recycling"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/recycal.png" alt=""></div>
		  </div>
		  <div class="col-md-12">
		   <h1><?php _e( 'BLOG', 'twentyseventeen' ); ?></h1>
		 </div>
	   </div>
	 </div>
	</section>
	<section class="blog_section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 blog_left_containt">
				
				<?php
				if ( have_posts() ) :

					/* Start the Loop */
					while ( have_posts() ) : the_post();

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'template-parts/post/content', get_post_format() );

					endwhile;	?>
				
					<?php	/*	the_posts_pagination( array(
						'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
						'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
						'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
					) );	*/
					?>
					<div class="pagination_section">
					<?php 	evince_pagination(); 	?>	
					</div>
					<?php
				else :

					get_template_part( 'template-parts/post/content', 'none' );

				endif;
				?>
				
				</div>
				<div class="col-md-4 blog_right_containt">
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
		</section>
	</div>
		
<?php get_footer();
