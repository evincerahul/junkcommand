<?php
/**
 * Displays footer widgets if assigned
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<?php
if ( is_active_sidebar( 'footer1' ) ||
	 is_active_sidebar( 'footer2' ) ) :
?>
			<div class="footer">
                <div class="container">
                  <div class="row">
                    <div class="col-lg-2  col-md-2 col-sm-4 col-xs-12 f-box">
						<?php
						if ( is_active_sidebar( 'footer1' ) ) { ?>
							<div class="widget-column footer-widget-1">
								<h3> OUR<br/>WEBSITE</h3>
								<?php dynamic_sidebar( 'footer1' ); ?>
							</div>
						<?php }	?>
                    </div>
                    <div class="col-lg-3  col-md-3 col-sm-4 col-xs-12 f-box">
                      <?php
						if ( is_active_sidebar( 'footer2' ) ) { ?>
							<h3> COMMERCIAL<br/>JUNK REMOVAL</h3>
							<div class="widget-column footer-widget-1">
								<?php dynamic_sidebar( 'footer2' ); ?>
							</div>
						<?php }	?>
                    </div>
                    <div class="col-lg-3  col-md-3 col-sm-4 col-xs-12 f-box">
                      <?php
						if ( is_active_sidebar( 'footer3' ) ) { ?>
							<h3> RESDENTIAL<br/>JUNK REMOVAL</h3>
							<div class="widget-column footer-widget-1">
								<?php dynamic_sidebar( 'footer3' ); ?>
							</div>
						<?php }	?>
                    </div>
                    <div class="col-lg-2  col-md-2 col-sm-4 col-xs-12 f-box">
                      <?php
						if ( is_active_sidebar( 'footer4' ) ) { ?>
							<h3> COMPANY<br/>INFO</h3>
							<div class="widget-column footer-widget-1">
								<?php dynamic_sidebar( 'footer4' ); ?>
							</div>
						<?php }	?>
                    </div>
                    <div class="col-lg-2  col-md-2 col-sm-6 col-xs-12 address-footer f-box">
                      <h3> CONTACT<br/>US</h3>
                      <span class="address">
                        Junk Command 7466 Washington Avenue South Eden Prairie, MN 55344
                      </span>
                      <span class="ph">PH: 952-495-5865</span>
                      <span class="social-icon">Follow us on :</span>
                      <ul class="social">
                        <li> <a href="https://www.facebook.com/pages/JUNK-COMMAND-INC/331029793837" target="_blank"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/facebook.png" alt=""></a> </li>
                        <li> <a href="https://twitter.com/JunkCommandInc" target="_blank"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/twitter.png" alt=""> </a> </li>
                        <li> <a href="#" target="_blank"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/google-p.png" alt=""> </a> </li>
                        <li> <a href="#" target="_blank"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/you-t.png" alt=""></a> </li>
                        <li> <a href="#" target="_blank"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/linkedin.png" alt=""> </a> </li>
                      </ul>
                    </div>
                  </div>
                  <!--/.row--> 
                </div>
                <!--/.container--> 
              </div>



	<aside class="widget-area" role="complementary">
		<?php
		if ( is_active_sidebar( 'sidebar-2' ) ) { ?>
			<div class="widget-column footer-widget-1">
				<?php dynamic_sidebar( 'sidebar-2' ); ?>
			</div>
		<?php }
		if ( is_active_sidebar( 'sidebar-3' ) ) { ?>
			<div class="widget-column footer-widget-2">
				<?php dynamic_sidebar( 'sidebar-3' ); ?>
			</div>
		<?php } ?>
	</aside><!-- .widget-area -->

<?php endif; ?>
