<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
		  <p class="pull-left">Copyright &copy; 2017 <a href="<?php echo esc_url( __( 'http://junkcommand.com', 'junkcommand' ) ); ?>" >JUNK command </a>All Rights Reserved. </p>
		  <div class="pull-right">
			<p class="pull-left"> POWERED BY :  <a href="<?php echo esc_url( __( 'https://www.evincedev.com', 'junkcommand' ) ); ?>" target="_blank"><?php printf( __( '%s', 'junkcommand' ), 'EVINCE DEVELOPMENT PVT . LTD.' ); ?></a></p>
		  </div>
		  