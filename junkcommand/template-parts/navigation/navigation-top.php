<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<nav id="site-navigation" class="navbar navbar-default" role="navigation" aria-label="<?php esc_attr_e( 'Top Menu', 'twentyseventeen' ); ?>">
	<div class="navbar-header">
	<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		<?php
		echo twentyseventeen_get_svg( array( 'icon' => 'bars' ) );
		echo twentyseventeen_get_svg( array( 'icon' => 'close' ) );
		_e( 'Menu', 'twentyseventeen' );
		?>
	</button>
	</div>
	
	<div id="navbar" class="no-padding-lr navbar-collapse collapse">
		<?php	//wp_nav_menu( array( 'container'=> false, 'menu_class'=> false,'items_wrap' => '<ul class="nav navbar-nav">%3$s<li id="moreLinks" class="moreLinks"><a href="javascript:;">More <i class="fa fa-angle-down"></i></a></li></ul>', 'theme_location'=> 'top' ) );
			//wp_nav_menu( array('theme_location' => 'top', 'items_wrap'=> '<ul class="nav navbar-nav"><li class="dropdown-toggle"><li><a>'.esc_html($menu_obj->name).'</a></li></ul><ul id="%1$s" class="%2$s">%3$s</ul>') );
                     $defaults = array(
                        'theme_location'  => 'top',
                        //'menu'            => '',
                         'container'       => false,
                         'container_class' => '',
                         'container_id'    => '',
                         'menu_class'      => 'nav navbar-nav',
                         'menu_id'         => 'menu-home-menu',
                         'walker'          =>  new My_Walker_Nav_Menu()
                    );
                    wp_nav_menu( $defaults );
		?>
	</div>
	<ul class="Schedule-btn">
		<li><a href="http://www.junkcommand.com/schedule-a-pickup">Schedule a Pick up</a></li>             
	</ul>
	<?php if ( ( twentyseventeen_is_frontpage() || ( is_home() && is_front_page() ) ) && has_custom_header() ) : ?>
		<a href="#content" class="menu-scroll-down"><?php echo twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ); ?><span class="screen-reader-text"><?php _e( 'Scroll down to content', 'twentyseventeen' ); ?></span></a>
	<?php endif; ?>
</nav><!-- #site-navigation -->
