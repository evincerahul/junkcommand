<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<div class="blog_detal" id="post-<?php the_ID(); ?>">
		<?php if ( '' !== get_the_post_thumbnail() ||  is_single() ) : ?>
		<div class="blog_img">
			<a href="<?php the_permalink(); ?>">
				<?php
					if ( has_post_thumbnail() ) {
						$src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false );	?>
						<img src='<?php	echo $src[0]; ?>'>
						
				<?php	}	?>
				<?php //the_post_thumbnail(); ?>
			</a>
		</div><!-- .post-thumbnail -->
	<?php endif; ?>
		<div class="blog_date_title">
			<?php the_time('j'); ?>
		<span><?php the_time('M'); ?></span>
		</div>
		<div class="blog_date_detail">
		<?php	the_title( '<h5 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h5>' );		
			if(is_single()){
				the_content();
			} else {
				echo get_field("post_sort_description");
			}
		?>
		</div>
		
		<?php
		/* translators: %s: Name of current post */
		//the_content();
		
		wp_link_pages( array(
			'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
			'after'       => '</div>',
			'link_before' => '<span class="page-number">',
			'link_after'  => '</span>',
		) );
		?>
		
	<?php	if (! is_single() ) {	?>	
	<div class="read_more_btn"><a href="<?php	echo esc_url( get_permalink());	 ?>">Read More</a></div>
	<?php	} else {	?>
		<div class="by_admin_text">
			<div class="admin_text"> <span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/admin_img.png"></span> By : <?php echo get_the_author();	?></div>
			<div class="comments_text"> <span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/comments_img.png"></span> <?php 	if($post->comment_count!=""){ echo $post->comment_count; ;	} else {	echo "0";	}	?> Comments</div>
		</div>
	<?php	}	?>

</div>
