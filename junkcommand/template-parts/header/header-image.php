<?php
/**
 * Displays header media
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 no-padding-left xs-padding-fifteen logo">
			<?php get_template_part( 'template-parts/header/site', 'branding' ); ?>
			</div>
			<div class="col-md-6 col-sm-6 text-right no-padding-right xs-padding-fifteen">
				<div class="contact-no">Schedule a Pick Up : <a href="tel:952.495.5865">952.495.5865</a></div>
			</div>
		</div>
		
	</div><!-- .container -->
