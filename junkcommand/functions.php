<?php
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array('parent-style')
    );
}
//remove logo class
add_filter('get_custom_logo','change_logo_class');
function change_logo_class($html)
{
	$html = str_replace('class="custom-logo"', '', $html);
	$html = str_replace('class="custom-logo-link"', '', $html);
	return $html;
}
	register_sidebar( array(
        'name' => __( 'CATEGORIES', 'junkcommand' ),
        'id' => 'category-sidebar',
        'description' => __( 'Add widgets here to appear in your footer.', 'junkcommand' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
	 register_sidebar( array(
        'name' => __( 'Footer1', 'junkcommand' ),
        'id' => 'footer1',
        'description' => __( 'Add widgets here to appear in your footer.', 'junkcommand' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
	 register_sidebar( array(
        'name' => __( 'Footer2', 'junkcommand' ),
        'id' => 'footer2',
        'description' => __( 'Add widgets here to appear in your footer.', 'junkcommand' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
	 register_sidebar( array(
        'name' => __( 'Footer3', 'junkcommand' ),
        'id' => 'footer3',
        'description' => __( 'Add widgets here to appear in your footer.', 'junkcommand' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
	register_sidebar( array(
        'name' => __( 'Footer4', 'junkcommand' ),
        'id' => 'footer4',
        'description' => __( 'Add widgets here to appear in your footer.', 'junkcommand' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
	register_sidebar( array(
        'name' => __( 'Footer5', 'junkcommand' ),
        'id' => 'footer5',
        'description' => __( 'Add widgets here to appear in your footer.', 'junkcommand' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');

//for pagination class
function evince_pagination() {
global $wp_query;
$big = 999999999; // need an unlikely integer
$pages = paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages,
        'type'  => 'array',
		
    ) );
    if( is_array( $pages ) ) {
        $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
        echo '<ul class="pagination">';
        foreach ( $pages as $page ) {
                echo "<li class='page-item'>$page</li>";
        }
       echo '</ul>';
        }
}
function theme_queue_js(){
    if ( 
              ! is_admin() 
           && is_singular() 
           && comments_open() 
           && get_option('thread_comments') 
    )
        wp_enqueue_script( 'comment-reply' );
}
add_action('wp_print_scripts', 'theme_queue_js');


function simple_comment_format($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment; ?>
    <?php if ( $comment->comment_approved == '1'): ?>
    <li <?php comment_class(); ?>>
        <article>
            <time><?php comment_date(); ?></time>
            <h4><?php comment_author(); ?></h4>
            <?php comment_text(); ?>
            <?php comment_reply_link(); ?>
        </article>
    <?php endif;
}
class My_Walker_Nav_Menu extends Walker_Nav_Menu {
    function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ){
      $GLOBALS['dd_children'] = ( isset($children_elements[$element->ID]) )? 1:0;
      $GLOBALS['dd_depth'] = (int) $depth;
      parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }
   function start_lvl(&$output, $depth) {
     $indent = str_repeat("\t", $depth);
     $output .= "\n$indent<ul class=\"dropdown\">\n";
   }
   
  function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) 
  {

    global $wp_query, $wpdb;
	
    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
    $li_attributes = '';
    $class_names = $value = '';
    $classes = empty( $item->classes ) ? array() : (array) $item->classes;
	//print_r($args);	die();
    //Add class and attribute to LI element that contains a submenu UL.
    if ($args->has_children){
      $classes[]    = 'dropdown';
      $li_attributes .= 'data-dropdown="dropdown"';
    }
    $classes[] = 'dropdown-toggle';
    //If we are on the current page, add the active class to that menu item.
    $classes[] = ($item->current) ? 'active' : '';
	
	 $items[1]->classes[] = 'first';
    $items[count($items)]->classes[] = 'last';
    
   
    //Make sure you still add all of the WordPress classes.
    $class_names = join( ' ', apply_filters( 'nav_menu_css_class',     array_filter( $classes ), $item, $args ) );
    $class_names = ' class="' . esc_attr( $class_names ) . '"';
    $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
    $id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';
    $has_children = $wpdb->get_var(
    $wpdb->prepare("
       SELECT COUNT(*) FROM $wpdb->postmeta
       WHERE meta_key = %s
       AND meta_value = %d
       ", '_menu_item_menu_item_parent', $item->ID)
     );
	 //echo $item->ID;
	$cnt = count($item);
	if($item->post_parent != 0){
		$classes[] = 'moreLinks'; 
	}
	if($id!=""){
	$output .= $indent . '<li' . $id . $value . $class_names .'>';
	$output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';
   }
   //Add attributes to link element.
   $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
   $attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target     ) .'"' : '';
   $attributes .= ! empty( $item->xfn ) ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
   $attributes .= ! empty( $item->url ) ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
   // Check if menu item is in main menu

if ( $depth == 0 && $has_children > 0  ) {
    // These lines adds your custom class and attribute
   // $attributes .= ' class="dropdown-toggle"';
    //$attributes .= ' data-toggle="dropdown"';
}
   $item_output = $args->before;
   $item_output .= '<a'. $attributes .'>';
   $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
   // Add the caret if menu level is 0
   if ( $depth == 0 && $has_children > 0  ) {
      $item_output .= ' <i class="fa fa-angle-down"></i>';
   }
   $item_output .= '</a>';
   $item_output .= $args->after;
   
   $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }
}


//for below code use of popular tag display in sidebar
