<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/owl.carousel.min.css" rel="stylesheet">
  <!-- Custom fonts for this template -->
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/font-awesome.min.css">    
  <!-- Custom styles for this template -->
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/style.css" rel="stylesheet">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/responsive.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> 
  
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="header-container">
	<header id="masthead" role="banner">
		<?php get_template_part( 'template-parts/header/header', 'image' ); ?>
	</header><!-- #masthead -->
	<?php if ( has_nav_menu( 'top' ) ) : ?>
		<div class="nav-menu">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-xs-12 xs-padding-fifteen no-padding-lr">
						<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
					</div><!-- .col-md-12 -->
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- .nav-menu -->
	<?php endif; ?>
	<div class="meganav-nav">
		<?php	wp_nav_menu( array( 'container'=> false, 'menu_class'=> false,'items_wrap' => '<ul class="">%3$s</ul>', 'theme_location'=> 'top','menu'=>'home-sub-menu' ) );	?>
	</div>
</div>
